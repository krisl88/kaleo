//
//  KSAddress.swift
//  CodeChallenge
//
//  Created by Kris Lau on 2/15/16.
//  Copyright © 2016 Kris Lau. All rights reserved.
//

import Foundation
import CoreData

@objc(KSAddress)
class KSAddress: KSManagedObject {

    override func updateWithResponseDictionary(dictionary: [NSObject : AnyObject]!) {
        self.suite = String(dictionary["suite"]!)
        self.city = String(dictionary["city"]!)
        self.zipcode = String(dictionary["zipcode"]!)
    }
}
