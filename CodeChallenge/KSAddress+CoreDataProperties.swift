//
//  KSAddress+CoreDataProperties.swift
//  CodeChallenge
//
//  Created by Kris Lau on 2/15/16.
//  Copyright © 2016 Kris Lau. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension KSAddress {

    @NSManaged var street: String?
    @NSManaged var suite: String?
    @NSManaged var zipcode: String?
    @NSManaged var city: String?
    @NSManaged var user: KSUser?

}
