//
//  DetailViewController.swift
//  CodeChallenge
//
//  Created by Kris Lau on 2/16/16.
//  Copyright © 2016 Kris Lau. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    var user: KSUser! {
        didSet (newUser) {
            self.updateUI()
        }
    }
    
    @IBOutlet weak var descLabel: UILabel!

    static func detailViewControllerWithUser(user: KSUser) -> DetailViewController{
        let vc = DetailViewController.instantiateViewControllerIdentifier("DetailViewController", storyboardName: "Main")
        vc.user = user
        return vc
    }
    
    func updateUI() {
        
        if let user = self.user{
            var str = ""
            str = user.name! + "\n" + user.username! + "\n" + user.email! + "\n" + user.website! + "\n" + user.phone!
            
            if let company = user.company
            {
                str += "\n\n" + company.name! + "\n" + company.catchPhrase! + "\n" + company.bs!
            }
            
            if let address = user.addresses!.anyObject()! as? KSAddress
            {
                str += "\n\n" + address.street! + " " + address.suite! + "\n" + address.city! + " " + address.zipcode!
            }
            if let label = self.descLabel {
                label.text = str.capitalizedString
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.updateUI()
    }

}

