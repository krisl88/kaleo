//
//  KSCompany.swift
//  CodeChallenge
//
//  Created by Kris Lau on 2/15/16.
//  Copyright © 2016 Kris Lau. All rights reserved.
//

import Foundation
import CoreData

@objc(KSCompany)
class KSCompany: KSManagedObject {

    override func updateWithResponseDictionary(dictionary: [NSObject : AnyObject]!) {
        self.catchPhrase = String(dictionary["catchPhrase"]!)
        self.bs = String(dictionary["bs"]!)
    }
}
