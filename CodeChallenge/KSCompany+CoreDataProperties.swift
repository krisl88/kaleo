//
//  KSCompany+CoreDataProperties.swift
//  CodeChallenge
//
//  Created by Kris Lau on 2/15/16.
//  Copyright © 2016 Kris Lau. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension KSCompany {

    @NSManaged var name: String?
    @NSManaged var catchPhrase: String?
    @NSManaged var bs: String?
    @NSManaged var user: NSSet?

}
