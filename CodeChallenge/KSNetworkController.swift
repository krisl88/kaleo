//
//  KSNetworkController.swift
//  CodeChallenge
//
//  Created by Kris Lau on 2/15/16.
//  Copyright © 2016 Kris Lau. All rights reserved.
//

class KSNetworkController {
    static let sharedInstance = KSNetworkController()
    let networkManager = AFHTTPRequestOperationManager()
    
   
    //NOTE: only have GET request for simiplicity. otherwise it'd have POST, PUT, and DELETE requests
    func getRequestWithPath(path: String, _ params: Dictionary<String,String>? = nil, completionHandler: CompletionHandler)
    {
        networkManager.GET( path,
            parameters: params,
            success: { (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) in
                completionHandler(.Success(responseObject))
            },
            failure: { (operation: AFHTTPRequestOperation!,error: NSError!) in
                completionHandler(.Failure(error))
        })
    }

    func cancelAllRequests()
    {
        networkManager.operationQueue.cancelAllOperations()
    }
}