//
//  KSTypeAlias.swift
//  CodeChallenge
//
//  Created by Kris Lau on 2/15/16.
//  Copyright © 2016 Kris Lau. All rights reserved.
//

enum CompletionResult {
    case Success(AnyObject?)
    case Failure(NSError?)
}
typealias CompletionHandler = (CompletionResult) -> Void

