//
//  KSUserController.swift
//  CodeChallenge
//
//  Created by Kris Lau on 2/15/16.
//  Copyright © 2016 Kris Lau. All rights reserved.
//

class KSUserController {
    static let sharedInstance = KSUserController()
    
    func fetchUsersWithCompletion(completionHandler: CompletionHandler)
    {
        let urlStr = "http://jsonplaceholder.typicode.com/users"
         KSNetworkController.sharedInstance.getRequestWithPath(urlStr) { (completionResult) -> Void in
            
            switch(completionResult)
            {
                case .Success(let responseObject):
                        completionHandler(.Success(self.usersFromResponse(responseObject as! NSArray)))
                case .Failure(_):
                    completionHandler(completionResult)
            }
        }
    }
    
    func usersFromResponse(response: NSArray) -> [KSUser]?{
        var users = [KSUser]()
        for dict in response
        {
            let id = String(dict["id"])
            let user = KSUser.findOrCreateWithUniqueValue(id, forKey: "uid", inManagedObjectContext: KSDataController.sharedInstance.managedObjectContext)
            user.updateWithResponseDictionary(dict as! [NSObject : AnyObject])
            users.append(user)
        }
        KSDataController.sharedInstance.saveContext()
        return users
    }
}
