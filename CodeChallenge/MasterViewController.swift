//
//  MasterViewController.swift
//  CodeChallenge
//
//  Created by Kris Lau on 2/16/16.
//  Copyright © 2016 Kris Lau. All rights reserved.
//

import UIKit
import CoreData

class MasterViewController: UITableViewController, NSFetchedResultsControllerDelegate {


    lazy var fetchedResultsController: NSFetchedResultsController = {
        let fetchRequest = KSUser.fetchRequestInManagedObjectContext(KSDataController.sharedInstance.managedObjectContext)
        let sortDescriptor = NSSortDescriptor(key: "uid", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: KSDataController.sharedInstance.managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
        
        return fetchedResultsController
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let userController = KSUserController.sharedInstance
        
        SVProgressHUD.show()
        userController.fetchUsersWithCompletion { (completionResult) -> Void in
            do {
                try self.fetchedResultsController.performFetch()
                self.tableView.reloadData()
                SVProgressHUD.dismiss()
            }
            catch
            {
                //NOTE: REALLY handle error
                SVProgressHUD.dismiss()
            }
        }
    }
    
    
    // MARK: - Table view data source
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return self.fetchedResultsController.sections?.count ?? 0
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionInfo = self.fetchedResultsController.sections![section]
        return sectionInfo.numberOfObjects
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("UserTableViewCell", forIndexPath: indexPath) as!UserTableViewCell
        
        let user = self.fetchedResultsController.objectAtIndexPath(indexPath) as! KSUser
        cell.configureWithUser(user)
        return cell
    }

    
    // MARK: - Segues

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {  
        if let indexPath = self.tableView.indexPathForSelectedRow {
            let user = self.fetchedResultsController.objectAtIndexPath(indexPath) as! KSUser
            let controller = (segue.destinationViewController as! UINavigationController).topViewController as! DetailViewController
            controller.user = user
        }
    }

   
}

