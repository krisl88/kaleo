//
//  UserTableViewCell.swift
//  CodeChallenge
//
//  Created by Kris Lau on 2/15/16.
//  Copyright © 2016 Kris Lau. All rights reserved.
//

import UIKit

class UserTableViewCell: UITableViewCell {

    func configureWithUser(user: KSUser)
    {
        let name = user.name!
        self.textLabel!.text = name
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
