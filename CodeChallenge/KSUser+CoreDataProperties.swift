//
//  KSUser+CoreDataProperties.swift
//  CodeChallenge
//
//  Created by Kris Lau on 2/15/16.
//  Copyright © 2016 Kris Lau. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension KSUser {

    @NSManaged var name: String?
    @NSManaged var username: String?
    @NSManaged var email: String?
    @NSManaged var phone: String?
    @NSManaged var website: String?
    @NSManaged var addresses: NSSet?
    @NSManaged var company: KSCompany?

}
