//
//  KSUser.swift
//  CodeChallenge
//
//  Created by Kris Lau on 2/15/16.
//  Copyright © 2016 Kris Lau. All rights reserved.
//

import Foundation
import CoreData

@objc(KSUser)
class KSUser: KSManagedObject {

    override func updateWithResponseDictionary(dictionary: [NSObject : AnyObject]!) {
        
        self.username = String(dictionary["username"]!)
        self.email = String(dictionary["email"]!)
        self.phone = String(dictionary["phone"]!)
        self.website = String(dictionary["website"]!)
        self.name = String(dictionary["name"]!)
        
        if let address = KSAddress.findOrCreateWithUniqueValue(dictionary["address"]!["street"], forKey: "street", inManagedObjectContext: KSDataController.sharedInstance.managedObjectContext)
        {
            address.updateWithResponseDictionary(dictionary["address"] as! [NSObject : AnyObject])
            self.addresses = [address]
        }
        
        if let company = KSCompany.findOrCreateWithUniqueValue(dictionary["company"]!["name"], forKey: "name", inManagedObjectContext: KSDataController.sharedInstance.managedObjectContext)
        {
            company.updateWithResponseDictionary(dictionary["company"] as! [NSObject : AnyObject])
            self.company = company
        }
    }
}
