//
//  KSViewController.swift
//  CodeChallenge
//
//  Created by Kris Lau on 2/15/16.
//  Copyright © 2016 Kris Lau. All rights reserved.
//

extension UIViewController
{
    class func instantiateViewControllerIdentifier(identifier: String, storyboardName: String) -> Self
    {
        return instantiateFromStoryboard(storyboardName, storyboardIdentifier: identifier)
    }
    
    private class func instantiateFromStoryboard<T>(storyboardName: String, storyboardIdentifier: String) -> T
    {
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        let controller = storyboard.instantiateViewControllerWithIdentifier(storyboardIdentifier) as! T
        return controller
    }
}